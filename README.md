# Deployment of Nginx loadbalacing with two application nodes.

The scripts are writtern in ansible. 

  - Install go Application on application nodes
  - Install Nginx in web node
  - Configure nginx and start the nginx 

### Version
1.0.1

### Dependencies


* Ansible
* Nginx
* Go go1.6.linux-amd64.tar.gz

### Usage

Invoke the install file which will call the other modules to install app, web(nginx) and configure nginx lb.

ansible-playbook install.yml 

